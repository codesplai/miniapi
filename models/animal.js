module.exports = (sequelize, DataTypes) => {
    const Animal = sequelize.define('Animal', {

        animal: DataTypes.STRING,
        foto: DataTypes.STRING,

    }, { tableName: 'rude_animales', timestamps: false });
    return Animal;
};
