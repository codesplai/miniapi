const express = require('express');
const app = express();

const model = require('./models/index');


app.get('/animales', function (req, res, next) {
    model.Animal.findAll()
        .then(animales => res.json(animales))
        .catch(error => res.json(error))
});


app.get('/animales/:id', function (req, res, next) {
    model.Animal.findOne({ where: { id: req.params.id } })
        .then(animal => res.json(animal))
        .catch(error => res.json(error))
});



app.listen(3000, () => console.log(`App en puerto 3000!`))
